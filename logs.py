
# this function reads all dara

def log_results(wd, data, log_array):
    try:
        log = wd.find_element_by_id("logResult").text
    except:
        log = 'Log element is not found'
    try:
        id = wd.find_element_by_id("testResult").text
    except:
        id = 'test result element is not found'
    result_messge = data["URLs"]  + " : " + "\n" + str(log) + "\n" + str(id) + "\n"
    log_array.append(result_messge)

    with open ('result', 'w+') as logfile:
        for d in log_array:
            logfile.write(d + '\n')