from selenium import webdriver


def open_browser (browser):
    # checking browser settings
    if str(browser) == 'chrome':
        wd = webdriver.Chrome('./old_modules/chromedriver')
        #chrome_driver = open("./chrome_settings.txt", 'r+')
        #wd = webdriver.Chrome(chrome_driver.read())
        #chrome_driver.close()
    elif str(browser) == 'firefox':
        wd = webdriver.Firefox()
    elif str(browser) == 'mobile':
        from selenium.webdriver.chrome.options import Options
        mobile_emulation = { "deviceName": "Apple iPhone 5" }
        chrome_options = Options()
        chrome_options.add_experimental_option("mobileEmulation", mobile_emulation)
        wd = webdriver.Chrome(chrome_options = chrome_options)
    else:
        print("Error: Browser is undefined")
    # here browser is waiting about 10 seconds untill element would be present
    wd.implicitly_wait(10)
    return wd
