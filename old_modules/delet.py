'''
my_array = [{'key1': 1 , 'key2' : 3}, {'key1': 3 , 'key2' : 2}, {'key1': 2 , 'key2' : 1}]
print(sorted(my_array, key = lambda x: x['key1']))
'''

import random
import string

print(''.join([random.choice(string.ascii_letters + string.digits + " "*10 + '\n') for n in range(100)]))

