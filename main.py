# As this is main module, I am using all other modules here

import go_to_url
from read_csv import data_from_csv, test_case_from_csv
import storeValue
import logs
import launch_browser
import JSModules


def main():
    log_array = []
    wd = None
    for csv_data in data_from_csv():
        for test_case_data in test_case_from_csv():
            if test_case_data['Action'] == 'Open browser':
                if wd == None:
                    wd = launch_browser.open_browser(browser='chrome')
            elif test_case_data['Action'] == 'Open URL':
                go_to_url.go_to_url(wd, csv_data['URLs'])
            elif test_case_data['Action'] == 'Trigger Rule':
                JSModules.trigger_rule(wd)
            elif test_case_data['Action'] == 'Click to C2C':
                JSModules.click_to_c2c(wd)
            elif test_case_data['Action'] == 'Send Parameter to JS':
                wd.execute_script(storeValue.storeValue(csv_data, wd))
            elif test_case_data['Action'] == 'JS Validation':
                try:
                    JSModules.js_validation(wd)
                except:
                    print('JS Validation error')
                    log_array.append('JS Validation error')
                    break
            elif test_case_data['Action'] == 'Send result to div':
                JSModules.send_results_to_DIV(wd)
            elif test_case_data['Action'] == 'Send Message':
                JSModules.send_message(wd)
            elif test_case_data['Action'] == 'Start chat from automaton':
                JSModules.start_chat_from_automaton(wd)
            elif test_case_data['Action'] == 'Automaton Check':
                JSModules.automaton_check(wd)
            elif test_case_data['Action'] == 'Close Chat':
                JSModules.end_rule(wd)
            else: print('"' + test_case_data['Action'] + '"' + ' is Wrong Action name')
        logs.log_results(wd, csv_data, log_array)
        print(log_array[-1])
    wd.quit()

main()
