from csv import DictReader
from os import listdir

# this function is reading all steps from test case csv file and return it as an array
def test_case_from_csv():
    with open("./test cases/tc1.csv" ) as csvfile:
        reader = DictReader(csvfile)
        test_case_csv_data = []
        for row in reader:
            test_case_csv_data.append(row)
        return test_case_csv_data


def data_from_csv():
# this part is reading all csv files from CSV folder
    csv1 = []
    for file1 in listdir('./CSV'):
        if file1.endswith(".csv"):
            csv1.append(file1)
# if there more or less then 1 csv it prints to the console
    if len(csv1) > 1:
        print("too many CSVs in current directory")
    elif len(csv1) < 1:
        print("CSV Not found")
# here it is reading all data from csv and returns it like array of dictionaries
    else:
        with open("./CSV/%s" % ''.join(csv1)) as csvfile:
            reader = DictReader(csvfile)
            data = []
            for row in reader:
                data.append(row)
    return data






'''
import csv

with open("./CSV/SQVT-EntertainmentCruises-pageMarker-TestCase.csv") as csvfile:
    reader = csv.DictReader(csvfile)
    data = []
    for row in reader:
        data.append(row)
'''