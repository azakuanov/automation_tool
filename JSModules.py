import time

def send_message(wd):
    with open("./JavaScript/sendMessage.js") as rule:
        return wd.execute_script(rule.read())


def send_results_to_DIV(wd):
    with open("./JavaScript/sendResultstoDIV.js") as rule:
        return wd.execute_script(rule.read())


def start_chat_from_automaton(wd):
    with open("./JavaScript/startChatFromAutomaton.js") as rule:
        return wd.execute_script(rule.read())


def trigger_rule(wd):
    with open("./JavaScript/triggerRule.js") as rule:
        time.sleep(3)
        return wd.execute_script(rule.read())


def automaton_check(wd):
    with open("./JavaScript/automatonCheck.js") as rule:
        return wd.execute_script(rule.read())


def end_rule(wd):
    with open("./JavaScript/endRule.js") as rule:
        return wd.execute_script(rule.read())


def js_validation(wd):
    with open("./JavaScript/capture&checkValues.js") as rule:
        return wd.execute_script(rule.read())

def click_to_c2c(wd):
    with open("./JavaScript/clickC2C.js") as rule:
        wd.execute_script(rule.read())
        time.sleep(5)