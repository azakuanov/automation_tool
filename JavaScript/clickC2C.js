function clickC2C() {
    try {
        if (inqFrame.Inq.C2CM._c2cList[0].image == "r" && typeof inqFrame.com === "undefined") {
            inqFrame.Inq.C2CM.agrty(0);
            storeValue.log.push(" C2C Launched");
        } else if (inqFrame.Inq.C2CM._c2cList[0].image == "b" || inqFrame.Inq.C2CM._c2cList[0].image == "ah") {
        	throw "C2C is either 'Busy' or 'After Hours'";
        	storeValue.log.push(" C2C is either 'Busy' or 'After Hours'");
        } else if (inqFrame.Inq.C2CM._c2cList[0].image == "r" && inqFrame.com.inq.flash.client.control.FlashVars.getValue("launchType") === "POPUP") {
            storeValue.log.push(" Business Rule is " + inqFrame.com.inq.flash.client.control.FlashVars.getValue("launchType") + "." + " Skipped ClickC2C.");
        }
    } catch (clickC2Cerr) {
        storeValue.log.push(" clickC2C Error: " + clickC2Cerr);
    }
}
clickC2C();
