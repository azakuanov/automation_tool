function automatonCheck() {
    setTimeout(function() {
        if (typeof inqGuide === "undefined") {
            storeValue.inqGuidePresent = false;
            storeValue.log.push(" Automaton check Successful. No Automaton Present");
        } else {
            storeValue.inqGuidePresent = true;
            storeValue.log.push(" Automaton check Successful. Automaton Present");
        }
    }, 5000);
}
automatonCheck();
