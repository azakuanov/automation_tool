function startChatFromAutomaton(nodeID) {
    if (storeValue.inqGuidePresent && storeValue.nodeTransition >= 1) {
        inqGuide.transitions.selectNextNode(nodeID);
        storeValue.log.push(" Transitioned to Node " + nodeID);
    } else {
        storeValue.log.push(" There is no Automaton Present");
    }
}
startChatFromAutomaton(storeValue.nodeTransition[0]);
