function changeNodes() {
    if (storeValue.nodeTransition >= 1 || storeValue.inqGuide !== false) {
        for (var a = storeValue.nodeTransition.length, b = 0; b < a; b++) {
            for (var c = inqGuide.node.nodes.length, d = 0; d < c; d++) {
                if (inqGuide.node.nodes[d] !== undefined && storeValue.nodeTransition[b] === parseInt(storeValue.nodeTransition[b], 10) && storeValue.nodeTransition[b] == inqGuide.node.nodes[d].node_id) {
                    inqGuide.transitions.selectNextNode(storeValue.nodeTransition[b]);
                    storeValue.log.push("Node transition to " + storeValue.nodeTransition[b] + " successful");
                    break;
                } else if (storeValue.nodeTransition[b] !== parseInt(storeValue.nodeTransition[b], 10) && storeValue.nodeTransition[b] == inqGuide.node.nodes[d].node_transition) {
                    inqGuide.transitions.evaluate(storeValue.nodeTransition[b]).then(function(node_id) {
                        inqGuide.transitions.selectNextNode(node_id);
                        return node_id;
                    })
                    storeValue.log.push("Node transition to " + storeValue.nodeTransition[b] + " successful");
                    break;
                }
            }
        }
    } else {
        storeValue.log.push("changeNodes() was skipped because nodeTransition = []");
    }
}
changeNodes();
