// The code below captures data stored in the Flash Variables of inqFrame. Automaton Vars uses Ternary Operators

try {
    window.captureFlashVars = {
        PersistentFrame: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("PersistentFrame") : "N/A",
        agAvail: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("agAvail") : "N/A",
        agID: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("agID") : "N/A",
        agName: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("agName") : "N/A",
        agent: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("agent") : "N/A",
        agentAttributes: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("agentAttributes") : "N/A",
        agentExitLine: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("agentExitLine") : "N/A",
        agentName: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("agentName") : "N/A",
        automatonSpecData: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("automatonSpecData") : "N/A",
        brID: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("brID") : "N/A",
        browserType: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("browserType") : "N/A",
        browserVersion: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("browserVersion") : "N/A",
        businessUnitID: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("businessUnitID") : "N/A",
        chatID: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("chatID") : "N/A",
        chatTitle: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("chatTitle") : "N/A",
        clickStream: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("clickStream") : "N/A",
        commTypes: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("commTypes") : "N/A",
        continued: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("continued") : "N/A",
        crHost: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("crHost") : "N/A",
        crPort: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("crPort") : "N/A",
        customerID: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("customerID") : "N/A",
        delay: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("delay") : "N/A",
        deviceType: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("deviceType") : "N/A",
        iframeURL: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("iframeURL") : "N/A",
        inHOP: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("inHOP") : "N/A",
        incAssignmentID: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("incAssignmentID") : "N/A",
        language: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("language") : "N/A",
        launchPageId: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("launchPageId") : "N/A",
        launchPageMarker: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("launchPageMarker") : "N/A",
        launchType: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("launchType") : "N/A",
        msgCnt: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("msgCnt") : "N/A",
        newFramework: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("newFramework") : "N/A",
        openerDelay: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("openerDelay") : "N/A",
        openerID: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("openerID") : "N/A",
        operatingSystemType: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("operatingSystemType") : "N/A",
        overrideAgentAlias: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("overrideAgentAlias") : "N/A",
        qt: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("qt") : "N/A",
        ruleAttributes: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("ruleAttributes") : "N/A",
        scriptID: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("scriptID") : "N/A",
        sessionID: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("sessionID") : "N/A",
        shutdownPopup: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("shutdownPopup") : "N/A",
        siteID: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("siteID") : "N/A",
        submitURL: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("submitURL") : "N/A",
        tagServerBaseURL: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("tagServerBaseURL") : "N/A",
        theEngagementType: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("theEngagementType") : "N/A",
        title: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("title") : "N/A",
        userAgent: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("userAgent") : "N/A",
        userName: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("userName") : "N/A",
        visitorAttributes: (typeof inqFrame.com !== "undefined") ? inqFrame.com.inq.flash.client.control.FlashVars.getValue("visitorAttributes") : "N/A",
        autoOpener: (function() { try {return inqFrame.com.inq.flash.client.chatskins.SkinControl.cw.arrayOpeners[0].Msg;} catch (e){return "N/A";}})(),
        c2cImageState: (typeof inqFrame.Inq.C2CM._c2cList[0] !== "undefined") ? inqFrame.Inq.C2CM._c2cList[0].image : "N/A",
        brName: (function() { if (typeof inqFrame.Inq.C2CM._c2cList[0] !== "undefined") { return inqFrame.Inq.C2CM._c2cList[0]._rule.name;} else if (inqFrame.Inq.CHM.chat !== null) { return inqFrame.Inq.CHM.chat.rule.name;} else { return "N/A";}})(),
        c2cSpecID: (typeof inqFrame.Inq.C2CM._c2cList[0] !== "undefined") ? inqFrame.Inq.C2CM._c2cList[0].c2cSpec.id.toString() : "N/A",
        c2cSpecName: (typeof inqFrame.Inq.C2CM._c2cList[0] !== "undefined") ? inqFrame.Inq.C2CM._c2cList[0].c2cSpec.name : "N/A",
        c2cThemeID: (typeof inqFrame.Inq.C2CM._c2cList[0] !== "undefined") ? inqFrame.Inq.C2CM._c2cList[0].c2cSpec.c2cTheme.id.toString() : "N/A",
        c2cThemeName: (typeof inqFrame.Inq.C2CM._c2cList[0] !== "undefined") ? inqFrame.Inq.C2CM._c2cList[0].c2cSpec.c2cTheme.name : "N/A",
        chatSpecID: (function() { if (typeof inqFrame.Inq.C2CM._c2cList[0] !== "undefined") { return inqFrame.Inq.C2CM._c2cList[0].c2cSpec.chatSpec.id.toString();} else if (inqFrame.Inq.CHM.chat !== null) { return inqFrame.Inq.CHM.chat.chatSpec.id.toString();} else { return "N/A";}})(),
        chatSpecName: (function() { if (typeof inqFrame.Inq.C2CM._c2cList[0] !== "undefined") { return inqFrame.Inq.C2CM._c2cList[0].c2cSpec.chatSpec.name;} else if (inqFrame.Inq.CHM.chat !== null) { return inqFrame.Inq.CHM.chat.chatSpec.name;} else { return "N/A";}})(),
        chatThemeID: (function() { if (typeof inqFrame.Inq.C2CM._c2cList[0] !== "undefined") { return inqFrame.Inq.C2CM._c2cList[0].c2cSpec.chatSpec.chatTheme.id.toString();} else if (inqFrame.Inq.CHM.chat !== null) { return inqFrame.Inq.CHM.chat.chatSpec.chatTheme.id.toString();} else { return "N/A";}})(),
        chatThemeName: (function() { if (typeof inqFrame.Inq.C2CM._c2cList[0] !== "undefined") { return inqFrame.Inq.C2CM._c2cList[0].c2cSpec.chatSpec.chatTheme.name;} else if (inqFrame.Inq.CHM.chat !== null) { return inqFrame.Inq.CHM.chat.chatSpec.chatTheme.name;} else { return "N/A";}})(),
        pageMarker: (typeof inqFrame !== "undefined") ? inqFrame.Inq.LDM.page.mID : "N/A",
        contentGroup: (function() {for (var cgName in inqFrame.Inq.LDM.contentGroups) { if (inqFrame.Inq.LDM.checkCG(cgName)) { return cgName;}}})(),
        automatonID: (typeof inqGuide !== "undefined") ? inqGuide.automaton.id.toString() : "N/A",
        automatonName: (typeof inqGuide !== "undefined") ? inqGuide.automaton.name : "N/A",
        nodeName: (typeof inqGuide !== "undefined") ? inqGuide.node.name : "N/A",
        nodeID: (typeof inqGuide !== "undefined") ? inqGuide.node.id.toString() : "N/A",
        URL: window.location.href
    };
} catch (er) {
    storeValue.log.push(er);
    throw "SQAT ERROR : " + er;
}


//This For loop takes the ordered values from storeValue.parameter and storeValue.expectedResults and structures these values in an Object called paramPlusExpResult. Param = Property, and ExpResult = Property Value

for (a = storeValue.parameter.length, b = 0; b < a; b++) {
    storeValue.paramPlusExpResult[storeValue.parameter[b]] = storeValue.expectedResult[b];
}

//This For loop evaluates paramPlusExpResult, and finds any properties that are named "". If they are found then the properties are removed from the Object

for (a = Object.keys(storeValue.paramPlusExpResult).length, b = 0; b < a; b++) {
    if (Object.keys(storeValue.paramPlusExpResult)[b] == "") {
        var prop = "";
        delete storeValue.paramPlusExpResult[prop];
    }
}

//This For loop identifies paramPlusExpResult's properties, and mirrors the properties from captureFlashVars into a new Object called compareFlashVars

for (c = Object.keys(captureFlashVars).length, d = 0; d < c; d++) {
    for (a = Object.keys(storeValue.paramPlusExpResult).length, b = 0; b < a; b++) {
        if (Object.keys(captureFlashVars)[d] == Object.keys(storeValue.paramPlusExpResult)[b]) {
            storeValue.compareFlashVars[Object.keys(captureFlashVars)[d]] = captureFlashVars[Object.keys(captureFlashVars)[d]];
        }
    }
}

//Now that paramPlusExpResult and compareFlashVars have the same properties, this For loop will compare their values. If matching, push Test Passed to storeValue.finalResults, else push Test Failed

for (var prop in storeValue.paramPlusExpResult) {
    if (storeValue.paramPlusExpResult[prop] == storeValue.compareFlashVars[prop]) {
        storeValue.finalResults[prop] = "TEST PASSED" + " | " + "Observed Result - " + storeValue.compareFlashVars[prop] + " = " + "Expected Result - " + storeValue.paramPlusExpResult[prop];
    } else {
        storeValue.finalResults[prop] = "TEST FAILED" + " | " + "Observed Result - " + storeValue.compareFlashVars[prop] + " =/= " + "Expected Result - " + storeValue.paramPlusExpResult[prop];
    }
}
