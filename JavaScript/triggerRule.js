function triggerRule(businessRule) {
	for (var c = inqFrame.Inq.BRM.rules, a = businessRule, b = 0, d = c.length; b <= d; b++) {
		if (b < d && a === c[b].name) {
			c[b].doActions(c[b]);
			storeValue.log.push(businessRule + " has fired");
			break;
		} else if (b == d) {
			var divResult = document.createElement("div");
			divResult.id = "testResult";
			divResult.innerHTML = "Business Rule " + businessRule + " not found";
			document.body.appendChild(divResult);
			storeValue.log.push("Business Rule " + businessRule + " not found");
		}
	}
}
triggerRule(storeValue.businessRule);
