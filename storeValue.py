from re import sub, search


def storeValue(data, wd):

    with open("./JavaScript/storeValue.js") as rule:

        regexp = '\"([0-9]+)\\"'
        original_js = rule.read()

        br_change = sub("pyBusinessRule", data["Business Rule"], original_js)
        # par
        param1 = sub("pyParam1", data["Parameter1"], br_change)
        param2 = sub("pyParam2", data["Parameter2"], param1)
        param3 = sub("pyParam3", data["Parameter3"], param2)
        param4 = sub("pyParam4", data["Parameter4"], param3)

        # expc
        expc1 = sub("pyExpc1", data["Expected Result1"], param4)
        if search(regexp, expc1) != None:
            expc1 = sub(regexp, data["Expected Result1"], expc1)
        expc2 = sub("pyExpc2", data["Expected Result2"], expc1)
        if search(regexp, expc2) != None:
            expc2 = sub(regexp, data["Expected Result2"], expc2)
        expc3 = sub("pyExpc3", data["Expected Result3"][0:-1], expc2)
        if search(regexp, expc3) != None:
            expc3 = sub(regexp, data["Expected Result3"][0:-1], expc3)
        expc4 = sub("pyExpc4", data["Expected Result4"], expc3)
        if search(regexp, expc4) != None:
            expc4 = sub(regexp, data["Expected Result4"], expc4)
        #print(expc4)
        return expc4



